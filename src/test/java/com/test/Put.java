package com.test;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class Put {
	@Test
	public void tesingPutRequest() {
		JSONObject js = new JSONObject();
		js.put("name", "ReshmaArshath");
		js.put("job", "Examiner");
		System.out.println(js);
		given().body(js.toJSONString()).put("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt",
				greaterThan("2023-01-24T09:59:40.438Z"));
	}

	@Test
	public void TestingPatchRequest() {
		JSONObject js = new JSONObject();
		js.put("name", "Arshath");
		js.put("job", "engineer");
		System.out.println(js);
		given().body(js.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt",
				greaterThan("2023-01-24T10:03:33.826Z"));
	}
}
