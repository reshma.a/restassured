package com.test;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class GivenWhen {
	@Test
	public void testStatuscode() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().statusCode(200);

	}

	@Test
	public void testparticularvalue() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().body("data[1].email", equalTo("lindsay.ferguson@reqres.in"));
	}

	@Test
	public void printAllvalues() {
		baseURI = "https://reqres.in/";
		given().get("/api/users?page=2").then().log().all();

	}
	@Test
	public void testparticularvalue2() {
		baseURI = "https://reqres.in/";
       given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));
	}
}
