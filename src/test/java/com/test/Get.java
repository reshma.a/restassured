package com.test;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Get {
	@Test
	public void firstGetmethod() {
		Response response = RestAssured.get("https://reqres.in/api/users?page=2");
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);
		System.out.println(response.getBody().asString());
	}

	@Test
	public void assertGetmethod() {
		Response response = RestAssured.get("https://reqres.in/api/users?page=2");
		int statusCode = response.getStatusCode();
		AssertJUnit.assertEquals(statusCode, 200);

	}

}
